# Harmony CloudDev Template (AGC)

___

This Project is template project which represents following AGC Services:

1. Auth Service
2. Cloud Function

### Initialization of AppGallery Connect SDK Services
___

Before using this services user must authenticate with phone number and code. After logged in, user can test
AGC services. First open up the application.

![](ScreenShots/HomePage/Home.png "Home Page")

## Auth Service

---

Auth Service helps you quickly build a secure and reliable user authentication system for your app by directly integrating cloud-based Auth Service capabilities into your app. To integrate the service, go to Auth Service Page on [AppGalleryConnect Portal](https://developer.huawei.com/consumer/cn/service/josp/agc/index.html#/), enable the mobile number authentication mode and log in with your number, then you can enjoy the Application with the unique and interactive user experience.

![Verification Step](ScreenShots/AuthPage/AuthEnable.png "Verify")

![Verification Step](ScreenShots/AuthPage/AuthVerify.png "Verify")

![Verification Step](ScreenShots/AuthPage/EnabledButtons.png "Verify")

![After login step](ScreenShots/AuthPage/AuthLoggedUser.png "Logged in User")

## Cloud Function

---

Cloud Functions enables serverless computing. It provides the Function as a Service (FaaS) capabilities to simplify app development and O&M by splitting service logic into functions and offers the Cloud Functions SDK that works with Cloud DB and Cloud Storage so that your app functions can be implemented more easily. Cloud Functions automatically scales in or out server resources for functions based on actual traffic, freeing you from server resource management and reducing your O&M workload.

![Run the Cloud Function Service](ScreenShots/FunctionPage/Function.png "Generate a unique id")

### Non Authenticated User

---

If user not authenticated login popup shows up and ask to user for login.

![Login Step](ScreenShots/AuthPage/LoginDialog.png "Login")

### Folder Structure

<pre>
.
└── entry/
    └── src/
        ├── main/
        │   ├── ets/
        │   │   ├── common/
        │   │   │   ├── Constants.ets
        │   │   │   └── CountryViewModel.ets
        │   │   ├── components/
        │   │   │   ├── image-picker/
        │   │   │   │   ├── Image.ts
        │   │   │   │   └── ImagePicker.ets
        │   │   │   ├── AuthDialog.ets
        │   │   │   ├── AuthResult.ets
        │   │   │   ├── CommonActionButton.ets
        │   │   │   └── HomeButton.ets
        │   │   ├── entryAbility/
        │   │   │   └── EntryAbility.ts
        │   │   ├── pages/
        │   │   │   ├── Auth.ets
        │   │   │   ├── CloudFunction.ets
        │   │   │   ├── CloudStorage.ets
        │   │   │   └── Index.ets
        │   │   └── services/
        │   │       ├── AgcConfig.ts
        │   │       ├── Auth.ts
        │   │       ├── Function.ts
        │   │       └── Storage.ts
        │   ├── resources/
        │   │   ├── base/
        │   │   │   ├── element/
        │   │   │   │   ├── color.json
        │   │   │   │   ├── float.json
        │   │   │   │   └── string.json
        │   │   │   ├── media
        │   │   │   └── profile/
        │   │   │       └── main_pages.json
        │   │   ├── en_US/
        │   │   │   └── element/
        │   │   │       └── string.json
        │   │   ├── rawfile/
        │   │   │   └── agconnectConfig.js
        │   │   └── zh_CN/
        │   │       └── element/
        │   │           └── string.json
        │   └── module.json5
        └── ohosTest
</pre>
